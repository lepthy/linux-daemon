#!/bin/bash

### BEGIN INIT INFO
# Provides:          <name>
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 6
# Short-Description: <s-desc>
# Description:       <desc>
### END INIT INFO

#set -x
#set -e

NAME=<name>

PIDFILE=<file.pid>
WORKDIR=<path>

PROGRAM=</path/bin>
ARGS=<args>

RED="\e[31m"
GREEN="\e[32m"
WHITE="\e[0m"

cecho() {
	echo -n -e $1
	echo -n $2
	echo -e "$WHITE"
}

start() {
	echo "Starting $NAME... "
	/sbin/start-stop-daemon --start --startas $PROGRAM --pidfile $PIDFILE --make-pidfile --chdir $WORKDIR --background -- $ARGS
	status=$?
	echo -n "Status: "
	if [ $status -ne 0 ]
		then
			cecho $RED "[Failed]"
	else
			cecho $GREEN "[ok]"
	fi
}

stop() {
	echo "Stopping $NAME..."
	/sbin/start-stop-daemon --stop --signal KILL --pidfile $PIDFILE >> /dev/null
	rm $PIDFILE >> /dev/null
}

run() {
	echo "Running $NAME..."
	cd $WORKDIR
	$PROGRAM $ARGS
}

if [ "$1" = "start" ]
	then
		start

elif [ "$1" = "stop" ]
	then
		stop

elif [ "$1" = "restart" ]
	then
		stop
		start

elif [ "$1" = "run" ]
	then
		run
else
	echo "Usage: $0 start|run|stop|restart"
fi
